package shipwire;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class InventoryAllocator{

	private final HashMap<String,Integer> inventory = new HashMap<String,Integer>();
	private Queue<Order> ordersQ = new LinkedList<Order>();
	private Thread allocatorThread;
	private List<int[]> finalResult = new ArrayList<int[]>();
	private Object lock = new Object();
	
	public InventoryAllocator(){
		inventory.put("A",150);
		inventory.put("B",150);
		inventory.put("C",100);
		inventory.put("D",100);
		inventory.put("E",200);
		allocatorThread =  new Thread(new Runnable(){
			public void run(){		
				while(shouldKeepRunning()){
					Order nextOrder;
					synchronized(lock){
						while(ordersQ.isEmpty()){
							try {
								lock.wait();
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
						}
						nextOrder = ordersQ.remove();
					}		
					boolean allocate;
					List<Line> lines = nextOrder.getLines();
					int header = nextOrder.getHeader();
					int[] result = new int[16];
					for(Line line : lines){
						String prod = line.getProduct();
						int quant = line.getQuantity();
						int currentQuantity = inventory.get(prod);
						if (currentQuantity >= quant){
							allocate = true;
							int newQuantity = currentQuantity - quant;
							inventory.put(prod, newQuantity);
							
						//backOrder
						}else{
							allocate = false;
						}
						allocateOrder(result,header,prod,quant,allocate);
						
					}
					finalResult.add(result);
				}
				for(int[] res : finalResult){
					print(res);
				}
			}
		});
		allocatorThread.start();
	}
	
	private void print(int[] result){
		StringBuilder sb = new StringBuilder();
		sb.append(result[0] + ": ");
		for(int i=1;i<5;i++){
			sb.append(result[i]+",");
		}
		sb.append(result[5]+"::");
		for(int j=6;j<10;j++){
			sb.append(result[j]+",");
		}
		sb.append(result[10]+"::");
		for(int k=11;k<15;k++){
			sb.append(result[k]+",");
		}
		sb.append(result[15]);
		System.out.println(sb.toString());
		
	}
	
	public void allocateInventory(String orders){
		parseOrders(orders);
	}
	
	private void parseOrders(String orders){
		try {
			boolean validOrder = false;
			JSONObject jsonOrders = new JSONObject(orders);
			int header = jsonOrders.getInt("Header");
			int streamId = jsonOrders.getInt("StreamId");
			JSONArray ords = jsonOrders.getJSONArray("Lines");
			List<Line> ordsAsList = new ArrayList<Line>();
			if (ords != null) { 
				for (int i=0; i<ords.length(); i++){ 
					JSONObject jsonLine = (JSONObject) ords.get(i);
					int quantity = jsonLine.getInt("Quantity");
					if(quantity < 0 || quantity > 5){
						validOrder = false;
						break;
					}
					if(quantity > 0 && quantity < 6 && !validOrder){
						validOrder = true;
					}
					String product = jsonLine.getString("Product");
					if(!this.inventory.containsKey(product)){
						validOrder = false;
						break;
					}
					Line line = new Line(product, quantity);
					ordsAsList.add(line);
				}
			}
			if (validOrder){
				Order newOrder = new Order(header, streamId, ordsAsList);
				synchronized(lock){
					if (ordersQ.isEmpty()){
						lock.notifyAll();
					}
					this.ordersQ.add(newOrder);
				}
			}			
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	
	
	private boolean shouldKeepRunning(){
		if (inventory.get("A").equals(0) 
				&& inventory.get("B").equals(0)
				&& inventory.get("C").equals(0) 
				&& inventory.get("D").equals(0)
				&& inventory.get("E").equals(0)){
			return false;			
		}
		
		return true;
	}
	//01234567890123456789012345678901234
	//1: 1,0,1,0,0::1,0,1,0,0::0,0,0,0,0 
	private void allocateOrder(int[] res,int header,String prod,int quant,boolean allocate){
		res[0] = header;
		if (prod.equals("A")){
			res[1] = quant;			
		}else if (prod.equals("B")){
			res[2] = quant;			
		}else if (prod.equals("C")){
			res[3] = quant;			
		}else if (prod.equals("D")){
			res[4] = quant;			
		}else if (prod.equals("E")){
			res[5] = quant;			
		}
		//Check if allocate or backorder
		if(allocate){
			if (prod.equals("A")){
				res[6] = quant;			
			}else if (prod.equals("B")){
				res[7] = quant;			
			}else if (prod.equals("C")){
				res[8] = quant;			
			}else if (prod.equals("D")){
				res[9] = quant;			
			}else if (prod.equals("E")){
				res[10] = quant;			
			}
		}else{
			if (prod.equals("A")){
				res[11] = quant;			
			}else if (prod.equals("B")){
				res[12] = quant;			
			}else if (prod.equals("C")){
				res[13] = quant;			
			}else if (prod.equals("D")){
				res[14] = quant;			
			}else if (prod.equals("E")){
				res[15] = quant;			
			}
		}
	}
	

}
