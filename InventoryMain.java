package shipwire;

import java.util.ArrayList;
import java.util.List;

public class InventoryMain {
	

	static InventoryAllocator ia = new InventoryAllocator();
	
	public static void todo1(){
		String s1 = "{\"Header\": 1, \"StreamId\": 1, \"Lines\": [{\"Product\": \"A\", \"Quantity\": \"5\"},{\"Product\": \"C\", \"Quantity\": \"5\"}]}";
		String s2 = "{\"Header\": 2, \"StreamId\": 1, \"Lines\": [{\"Product\": \"E\", \"Quantity\": \"5\"}]}";
		String s3 = "{\"Header\": 3, \"StreamId\": 1, \"Lines\": [{\"Product\": \"D\", \"Quantity\": \"5\"}]}";
		String s4 = "{\"Header\": 4, \"StreamId\": 1, \"Lines\": [{\"Product\": \"A\", \"Quantity\": \"5\"},{\"Product\": \"C\", \"Quantity\": \"5\"}]}";
		String s5 = "{\"Header\": 5, \"StreamId\": 1, \"Lines\": [{\"Product\": \"B\", \"Quantity\": \"5\"}]}";
		String s6 = "{\"Header\": 6, \"StreamId\": 1, \"Lines\": [{\"Product\": \"D\", \"Quantity\": \"5\"}]}";
		List<String> orders = new ArrayList<String>();
		orders.add(s1);
		orders.add(s2);
		orders.add(s3);
		orders.add(s4);
		orders.add(s5);
		orders.add(s6);
		for (String s : orders){
			ia.allocateInventory(s);
		}
	}
	
	public static void todo2(){
		String s1 = "{\"Header\": 13, \"StreamId\": 2, \"Lines\": [{\"Product\": \"A\", \"Quantity\": \"5\"},{\"Product\": \"C\", \"Quantity\": \"5\"}]}";
		String s2 = "{\"Header\": 14, \"StreamId\": 2, \"Lines\": [{\"Product\": \"E\", \"Quantity\": \"5\"}]}";
		String s3 = "{\"Header\": 15, \"StreamId\": 2, \"Lines\": [{\"Product\": \"D\", \"Quantity\": \"5\"}]}";
		String s4 = "{\"Header\": 16, \"StreamId\": 2, \"Lines\": [{\"Product\": \"A\", \"Quantity\": \"5\"},{\"Product\": \"C\", \"Quantity\": \"5\"}]}";
		String s5 = "{\"Header\": 17, \"StreamId\": 2, \"Lines\": [{\"Product\": \"B\", \"Quantity\": \"5\"}]}";
		String s6 = "{\"Header\": 18, \"StreamId\": 2, \"Lines\": [{\"Product\": \"D\", \"Quantity\": \"5\"}]}";
		List<String> orders = new ArrayList<String>();
		orders.add(s1);
		orders.add(s2);
		orders.add(s3);
		orders.add(s4);
		orders.add(s5);
		orders.add(s6);
		for (String s : orders){
			ia.allocateInventory(s);
		}
	}
	
	public static void todo3(){
		String s1 = "{\"Header\": 7, \"StreamId\": 3, \"Lines\": [{\"Product\": \"A\", \"Quantity\": \"5\"},{\"Product\": \"C\", \"Quantity\": \"5\"}]}";
		String s2 = "{\"Header\": 8, \"StreamId\": 3, \"Lines\": [{\"Product\": \"E\", \"Quantity\": \"5\"}]}";
		String s3 = "{\"Header\": 9, \"StreamId\": 3, \"Lines\": [{\"Product\": \"D\", \"Quantity\": \"5\"}]}";
		String s4 = "{\"Header\": 10, \"StreamId\": 3, \"Lines\": [{\"Product\": \"A\", \"Quantity\": \"5\"},{\"Product\": \"C\", \"Quantity\": \"5\"}]}";
		String s5 = "{\"Header\": 11, \"StreamId\": 3, \"Lines\": [{\"Product\": \"B\", \"Quantity\": \"5\"}]}";
		String s6 = "{\"Header\": 12, \"StreamId\": 3, \"Lines\": [{\"Product\": \"D\", \"Quantity\": \"5\"}]}";
		List<String> orders = new ArrayList<String>();
		orders.add(s1);
		orders.add(s2);
		orders.add(s3);
		orders.add(s4);
		orders.add(s5);
		orders.add(s6);
		for (String s : orders){
			ia.allocateInventory(s);
		}
	}

	public static void main(String[] args) {
		Thread t1 = new Thread(new Runnable() {
			public void run() {
				todo1();
			}
		});
		Thread t2 = new Thread(new Runnable() {
			public void run() {
				todo2();
			}
		});
		Thread t3 = new Thread(new Runnable() {
			public void run() {
				todo3();
			}
		});
		t1.start();
		t2.start();
		t3.start();
	}

}
