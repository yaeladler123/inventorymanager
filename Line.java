package shipwire;

public class Line {
	
	private String product;
	private int quantity;
	
	public Line(){
		
	}
	
	public Line(String p, int q){
		this.product = p;
		this.quantity = q;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	

}
