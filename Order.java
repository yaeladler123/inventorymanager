/**
 * 
 */
package shipwire;

import java.util.List;

public class Order {
	
	private int header;
	private int streamId;
	private List<Line> lines;
	
	public Order(int h, int sId, List<Line> o){
		this.header = h;
		this.streamId = sId;
		this.lines = o;		
	}

	public int getHeader() {
		return header;
	}

	public void setHeader(int header) {
		this.header = header;
	}

	public int getStreamId() {
		return streamId;
	}

	public void setStreamId(int streamId) {
		this.streamId = streamId;
	}

	public List<Line> getLines() {
		return lines;
	}

	public void setLines(List<Line> lines) {
		this.lines = lines;
	}
	
	

}
